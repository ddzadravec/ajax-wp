jQuery(function($){

	/********************************
	*	Listar Posts
	********************************/
	var listarPostsAjax = function(){
		
		$.ajax({
			url: wp.ajaxurl,
			type: 'GET',
			data:{
				action: 'listarPosts',
			},
			beforeSend:function(){
				$('.progress').removeClass('d-none');
			}
		})
		.done(function(resposta){
			$('.progress').addClass('d-none');
			$('#lista-posts').html(resposta);
		})
		.fail(function(){
			console.log("algo deu ruim aqui no listarPostsAjax");
		})
		
	}


	// Ação do botao da categoria

	$('.list-group-item').on('click', function(){
		listarPostsAjax();
		$('.list-group-item').removeClass('active');
		$(this).addClass('active');
	})

	// Ação do botao da paginação

	$('.page-item').on('click', function(){
		listarPostsAjax();
		$('.page-item').removeClass('active');
		$(this).addClass('active');
	})
	


	listarPostsAjax();


	/********************************
	*	Detalhe Post
	********************************/

	var detalhesPostAjax = function(){

		$.ajax({
			url: wp.ajaxurl,
			type: 'GET',
			data:{
				action: 'detalhesPost',
			},
			beforeSend:function(){
				$('.progress').removeClass('d-none');
			}
		})
		.done(function(resposta){
			$('.progress').addClass('d-none');
		})
		.fail(function(){
			console.log("algo deu ruim aqui no detalhesPostAjax");
		})

	}

	// Ação do botao leia mais

	$('.btn-detalhes').on('click', function(){
		detalhesPostAjax();
	})

	//detalhesPostAjax();

	/********************************
	*	Curtir e Descurtir Posts
	********************************/

	var curtirPostToggleAjax = function(){

		$.ajax({
			url: wp.ajaxurl,
			type: 'GET',
			data:{
				action: 'curtirPostToggle',
			},
			beforeSend:function(){
				$('.progress').removeClass('d-none');
			}
		})
		.done(function(resposta){
			$('.progress').addClass('d-none');
		})
		.fail(function(){
			console.log("algo deu ruim aqui no curtirPostToggleAjax");
		})

	}

	// Ação do botao Curtir

	$('.btn-curtir').on('click', function(){
		curtirPostToggleAjax();
	})

	//curtirPostToggleAjax();













})